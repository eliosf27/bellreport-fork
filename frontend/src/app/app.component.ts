import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'base/templates/app.component.html',
  styleUrls: ['base/styles/app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
