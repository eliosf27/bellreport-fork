# Reports Project

## Without Docker

### BACKEND 
#### Configuration:

1. Fork the project on GitLab and clone it to your local respository:
            
        git clone git@gitlab.bellbank.com:utilities/reportsystem.git

2. Config python enviroment:

Choose only one option to work with python enviroment

* Config Virtualenv Wrapper:

        a)  Install dependecies:
            sudo apt install python-pip
            sudo pip install virtualenvwrapper
          
        b)  Config dependencies:
            * Open the bashrc file:
                vim ~/.bashrc
            * Copy the following at the end of the file    
                export WORKON_HOME=$HOME/.virtualenvs
                source /usr/local/bin/virtualenvwrapper.sh

* Config Virtualenv (Optional):
        
        virtualenv -p python3 reports_project_env
        source reports_project_env/bin/activate

3. Create the virtual enviroment:
        
         mkvirtualenv reports_project_env
         
4. Activate the enviroment:
         
         workon reports_project_env
         
5. Install the requirements:
         
         pip install -r backend/requirements.txt

### Tasks
* Run tests:

        pytest --ds=reports_project.settings
        
* Run PEP8 validations:

        flake8
        
* Verify if the imports are correctly sorted 
        
        isort -c -rc -df
        
Note: We can run all the tests and the validations with a single command:
        
        ./test-all.sh

#### Run development server:
        
        export DJANGO_SETTINGS_MODULE=reports_project.settings
        python manage.py runserver
        
### FRONTEND
#### Configuration

1. Install the dependencies:
        
        cd frontend ; npm install
        
2. Run the development server
        
        npm start
        
        
## With Docker
0. Install and configure Docker

    * Install docker. [Docker](https://www.docker.com)

    * Install docker-compose. [Compose](https://docs.docker.com/compose/install/)


1. Build the containers: 

        docker-compose build
        
        
2. Run the containers
        
        docker-compose up
        
 